;; *********************************************************************
;;
;; interp.scm is part of the 2732A EPROM Programmer project
;;
;; Released as free software by permission of Tatonduk Outfitters Limited
;;
;; SPDX-FileCopyrightText: 2021 Christopher Howard <christopher@librehacker.com>
;; SPDX-FileCopyrightText: 2021 Tatonduk Outfitters Limited
;; SPDX-License-Identifier: GPL-3.0-or-later
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.
;;
;; This module deals with procedures that process parts of text to and
;; from the FlashForth interpreter.
;;
;; *********************************************************************

(define-module (ff5 interp)
  #:use-module (ice-9 textual-ports)
  #:use-module (ice-9 binary-ports)
  #:use-module (ff5 uart)
  #:export (txe
	    eat-whitespace
	    get-non-whitespace
	    recv-input-token
	    flush-to-eol
	    flush-all-available))

(define eol-string (string #\return #\linefeed))

;; *********************************************************************
;; `txe' transmits str, as well as a CRLF. FlashForth echos all the
;; characters you transmit except CR and LF, so we have to discard an
;; appropriate number of characters afterwards from the input stream.
;; *********************************************************************

(define (txe uart str)
  (put-string (uart-out-port uart) (string-append str eol-string))
  (force-output (uart-out-port uart))
  (get-string-n (uart-in-port uart) (string-length str)))

;; *********************************************************************
;; Conveniently in FORTH, "syntax" comes down simply to processing
;; whitespace-separated tokens.
;; *********************************************************************

(define (eat-whitespace uart)
  (if (char-whitespace? (lookahead-char (uart-in-port uart)))
      (begin (get-char (uart-in-port uart)) (eat-whitespace uart))))

(define (get-non-whitespace uart accum-str)
    (if (char-whitespace? (lookahead-char (uart-in-port uart)))
	accum-str
	(get-non-whitespace
	 uart
	 (string-append accum-str (string (get-char (uart-in-port uart)))))))

(define (recv-input-token uart)
  (eat-whitespace uart)
  (get-non-whitespace uart (string)))

;; *********************************************************************
;; flush-to-eol is helpful for discarding FF post-processing output
;; ("ok, etc."). flush-all-available is useful for discarding garbage in
;; the input stream before sending a command. flush-all-available does
;; introduce a timeout delay, however. The timeout value could be
;; tweaked.
;; *********************************************************************

(define (flush-to-eol uart)
  (if (not (eq? (get-char (uart-in-port uart)) #\newline))
      (flush-to-eol uart)))

(define (flush-all-available uart)
  (if (not (null? (car (select (list (uart-in-port uart)) '() '() 0 10000))))
      (begin
	(get-char (uart-in-port uart))
	(flush-all-available uart))))
