;; *********************************************************************
;;
;; uart.scm is part of the 2732A EPROM Programmer project
;;
;; Released as free software by permission of Tatonduk Outfitters Limited
;;
;; SPDX-FileCopyrightText: 2021 Christopher Howard <christopher@librehacker.com>
;; SPDX-FileCopyrightText: 2021 Tatonduk Outfitters Limited
;; SPDX-License-Identifier: GPL-3.0-or-later
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.
;;
;; This module handles setting up an interface to interact with
;; FlashForth through ACM usb->UART.
;;
;; Assumes:
;; - /dev/ttyACM0
;; - 38400 1 N comm settings
;;
;; non-guile dependencies:
;; - picocom
;; - bash shell
;;
;; Internally what connect-uart does is
;; 1. create FIFO files
;; 2. launches picocom and ties it to the FIFO files
;; 3. creates i/o ports to the FIFO files
;; 4. stuffs them into a <uart> object which is returned to the user
;;
;; The user uses `uart-in-port' and `uart-out-port' accessors to get
;; i/o ports from the <uart> object. `close-uart' shuts down picocom,
;; closes the ports, and removes the FIFO files.
;;
;; *********************************************************************

(define-module (ff5 uart)
  #:use-module (ice-9 popen)
  #:use-module (ice-9 receive)
  #:use-module (ice-9 textual-ports)
  #:use-module (srfi srfi-9)
  #:export (uart-in-port
	    uart-out-port
	    connect-uart
	    close-uart))

(define (num->hex-str n) (number->string n 16))

(define (rand-file-prefix) (num->hex-str (random 4294967295)))

(define (create-fifos)
  (let* ((prefix (rand-file-prefix))
	 (in-path (string-append "/tmp/in-" prefix))
	 (out-path (string-append "/tmp/out-" prefix)))
    (mknod in-path 'fifo #o600 0)
    (mknod out-path 'fifo #o600 0)
    (values in-path out-path)))

(define (launch-picocom in-path out-path)
  (system
   (string-append "picocom -b 38400 -p 1 -q /dev/ttyACM0 > " in-path
		  "< " out-path " &" )))

(define (create-file-ports in-path out-path)
  (values (open-input-file in-path) (open-output-file out-path)))

(define (rm-files in-path out-path) (system* "rm" in-path out-path))

(define-record-type <uart>
  (make-uart in-path out-path in-port out-port)
  uart?
  (in-path uart-in-path)
  (out-path uart-out-path)
  (in-port uart-in-port)
  (out-port uart-out-port))

(define (connect-uart)
  (call-with-values create-fifos
    (lambda (in-path out-path)
      (launch-picocom in-path out-path)
      (call-with-values
	  (lambda () (create-file-ports in-path out-path))
	(lambda (in-port out-port)
	  (make-uart in-path out-path in-port out-port))))))

(define (send-picocom-done out-port)
  (put-string out-port (string #\soh #\can))
  (force-output out-port))

(define (close-uart u)
  (send-picocom-done (uart-out-port u))
  (close-port (uart-in-port u))
  (close-port (uart-out-port u))
  (rm-files (uart-in-path u) (uart-out-path u)))
