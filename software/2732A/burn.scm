;; *********************************************************************
;;
;; burn.scm is part of the 2732A EPROM Programmer project
;;
;; Released as free software by permission of Tatonduk Outfitters Limited
;;
;; SPDX-FileCopyrightText: 2021 Christopher Howard <christopher@librehacker.com>
;; SPDX-FileCopyrightText: 2021 Tatonduk Outfitters Limited
;; SPDX-License-Identifier: GPL-3.0-or-later
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.
;;
;; The purpose of this module is to provide the `burn-range' procedure,
;; which takes bytes from input-port, and burns them to the chip at the
;; memory addresses between start-addr and end-addr, inclusively. The
;; firmware returns a verification byte after each byte is burned, and
;; burn-range will throw error 'bad-verify if this does not match the
;; original input value. `burn-range' also needs a <uart> object from
;; (ff5 uart). The input file port should be opened in binary mode.
;;
;; *********************************************************************

(define-module (2732A burn)
  #:use-module (ice-9 textual-ports)
  #:use-module (ice-9 binary-ports)
  #:use-module (ff5 uart)
  #:use-module (ff5 interp)
  #:re-export (connect-uart
	       close-uart)
  #:export (burn-range))

(define (send-burn-init-cmd uart)
  (txe uart (string-append "first-burn-init"))
  (force-output (uart-out-port uart)))

(define (send-burn-cmd uart byte addr)
  (txe
   uart
   (string-append
    "$" (number->string byte 16) " $" (number->string addr 16) " burn/verify hex .")))

(define (check-addresses curr-addr end-addr)
  (if (or (< curr-addr 0) (> curr-addr #xfff)
	  (< end-addr 0) (> end-addr #xfff))
      (throw 'out-of-range)))

(define (check-verify-result input-byte verification-token)
  (if (not (eq? input-byte (string->number verification-token 16)))
      (throw 'bad-verify)))

(define (burn-range_ curr-addr end-addr uart input-port)
  (if (<= curr-addr end-addr)
      (begin
	(let ((input-byte (get-u8 input-port)))
	(send-burn-cmd uart input-byte curr-addr)
	(let ((itok (recv-input-token uart)))
	  (check-verify-result input-byte itok)
	  (flush-to-eol uart)
	  (burn-range_
	   (1+ curr-addr)
	   end-addr uart input-port))))))

(define (burn-range start-addr end-addr uart input-port)
  (check-addresses start-addr end-addr)
  (flush-all-available uart)
  (send-burn-init-cmd uart)
  (flush-to-eol uart)
  (burn-range_ start-addr end-addr uart input-port))
