;; *********************************************************************
;;
;; read.scm is part of the 2732A EPROM Programmer project
;;
;; Released as free software by permission of Tatonduk Outfitters Limited
;;
;; SPDX-FileCopyrightText: 2021 Christopher Howard <christopher@librehacker.com>
;; SPDX-FileCopyrightText: 2021 Tatonduk Outfitters Limited
;; SPDX-License-Identifier: GPL-3.0-or-later
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.
;;
;; This module deals with reading a range of memory locations from the
;; 2732A. read-range is the interface procedure, which takes a start
;; memory address, and end memory address, a uart object from
;; (ff5 uart), and a "store" output port to send the data to. The data
;; is sent a single byte at a time (excluding buffering effects) to the
;; store port in binary mode. No textual formatting or other padding
;; is done to the output data. The output file port should be opened in
;; binary mode.
;;
;; *********************************************************************

(define-module (2732A read)
  #:use-module (ice-9 textual-ports)
  #:use-module (ice-9 binary-ports)
  #:use-module (ff5 uart)
  #:use-module (ff5 interp)
  #:re-export (connect-uart
	       close-uart)
  #:export (read-range))

(define (send-read-init-cmd uart)
  (txe uart (string-append "first-read-init"))
  (force-output (uart-out-port uart)))

(define (send-read-cmd uart addr)
  (txe
   uart
   (string-append
    "$" (number->string addr 16) " read hex .")))

(define (store-read-result itok store-port)
  (let ((rawnum (string->number itok 16)))
    (if (or (< rawnum 0) (> rawnum 255))
	(throw 'bad-storage-value)
	(put-u8 store-port rawnum))))

(define (read-range_ curr-addr end-addr uart store-port)
  (if (<= curr-addr end-addr)
      (begin
	(send-read-cmd uart curr-addr)
	(let ((itok (recv-input-token uart)))
	  (store-read-result itok store-port)
	  (flush-to-eol uart)
	  (read-range_
	   (1+ curr-addr)
	   end-addr uart store-port)))
      (force-output store-port)))

(define (read-range curr-addr end-addr uart store-port)
  (flush-all-available uart)
  (send-read-init-cmd uart)
  (flush-to-eol uart)
  (read-range_ curr-addr end-addr uart store-port))
