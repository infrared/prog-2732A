# Copyright and Licensing

[![REUSE status](https://api.reuse.software/badge/codeberg.org/infrared/prog-2732A)](https://api.reuse.software/info/codeberg.org/infrared/prog-2732A)

README.md is part of the 2732A EPROM Programmer project

Released as free software by permission of Tatonduk Outfitters Limited

SPDX-FileCopyrightText: 2021 Christopher Howard <christopher@librehacker.com>

SPDX-FileCopyrightText: 2021 Tatonduk Outfitters Limited

SPDX-License-Identifier: GPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or (at
your option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see https://www.gnu.org/licenses/.

# Requirements

## Hardware

- ELEGOO MEGA2560 R3 or compatible board
- 2732A EPROM PROGRAMMER board REV C. See schematic in `hardware` folder.

Note that the timing for the 2732A *burn* firmware code is by default
setting to the timing for the TMS2732AJL-45 model chip. Older chips
used a longer burn pulse, so you couldn't burn to the older chips
without changing the PULSE_WIDTH variable in the burn.fs and
recompiling the burn module.

Any model chip should work for *reading* chip memory, so long as the
chip is 2732A compatible.

## FlashForth

- [FlashForth 5](https://flashforth.com) loaded on a MEGA2560
- FF5 version 18.11.2020 was used in development.

An ihex binary for FF5 18.11.2020 is available from
https://github.com/oh2aun/flashforth.git, commit c947663, at path
avr/hex/2560-16MHz-38400.hex.

## Desktop Operating System Environment

Install Gnu Guix System, Stable release, for x86_64

## Desktop Software

- GNU Guile 3.0.7
- picocom v3.1

When logged in to Guix, run

    $ guix pull --commit=9703a51
    $ guix install guile picocom

# Compiling the MEGA2560 R3 code

After setting up a FlashForth shell connection, run these commands in
the shell.

    #send util
    #send 2560defs
    #send 2732Aio
    #send burn

# Dumping 2732A memory to a file

## Procedure

    $ cd eprom/prog-2732A/software
    $ guile
    scheme@(guile-user)> (set! %load-path (cons "." %load-path))
    scheme@(guile-user)> (use-modules (2732A read))
    scheme@(guile-user)> (define u (connect-uart))
    scheme@(guile-user)> (define op (open-file "out.bin" "wb"))
    scheme@(guile-user)> (read-range 0 #xfff u op)
    scheme@(guile-user)> (close-port op)
    scheme@(guile-user)> (close-uart u)
    scheme@(guile-user)> ,q

File will be called "out.bin" in the eprom/prog-2732A/software
directory.

## Data output format

   The data file outputted will consist of 4096 bytes of data, with
   each byte matching the data in the eprom chip from memory address
   0x0 (0) to memory address 0xfff (4095).

# Burning a file to 2732A memory

## Procedure

Data to burn should be in input file "in.bin".

    $ cd eprom/prog-2732A/software
    $ guile
    scheme@(guile-user)> (set! %load-path (cons "." %load-path))
    scheme@(guile-user)> (use-modules (2732A burn))
    scheme@(guile-user)> (define u (connect-uart))
    scheme@(guile-user)> (define ip (open-file "in.bin" "rb"))
    scheme@(guile-user)> (burn-range 0 #xfff u ip)
    scheme@(guile-user)> (close-port ip)
    scheme@(guile-user)> (close-uart u)
    scheme@(guile-user)> ,q

## Input file format

The data file for input should consist of 4096 bytes of data, with
each byte matching the data to be loaded into the eprom chip from
memory address 0x0 (0) to memory address 0xfff (4095).

Note that if you were to adjust the burn-range call to burn a smaller
range of memory, you would need a smaller input file to match. The
program does not seek around in the input file to match the burn
address locations - it simply treats the file like a stream of bytes
to pull from.

## Bugs

In theory, `burn-range' should check a verification-read byte after
each byte is burned, and throw an error if it does not match. But I am
not sure this is working correctly, i.e., it may not be throwing
errors that it should. I'm not sure if this is a problem with the
firmware or with the board design. I recommend doing a full chip read
after each full chip burn, and comparing the input and output file
checksums.