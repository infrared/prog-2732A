\ **********************************************************************
\
\ util.fs is part of the 2732A EPROM Programmer project
\
\ Released as free software by permission of Tatonduk Outfitters Limited
\
\ SPDX-FileCopyrightText: 2021 Christopher Howard <christopher@librehacker.com>
\ SPDX-FileCopyrightText: 2021 Tatonduk Outfitters Limited
\ SPDX-License-Identifier: GPL-3.0-or-later
\
\ This program is free software: you can redistribute it and/or modify
\ it under the terms of the GNU General Public License as published by
\ the Free Software Foundation, either version 3 of the License, or
\ (at your option) any later version.
\
\ This program is distributed in the hope that it will be useful,
\ but WITHOUT ANY WARRANTY; without even the implied warranty of
\ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
\ GNU General Public License for more details.
\
\ You should have received a copy of the GNU General Public License
\ along with this program.  If not, see <https://www.gnu.org/licenses/>.
\
\ This module provides a set of convenience words that are broadly
\ useful throughout the project codebase.
\
\ public words:
\   cp>r get-bit
\
\ **********************************************************************

util
marker util

: cp>r ( u -- u : -- u ) dup >r ; inlined

: get-bit ( u u -- b ) cp>r #1 swap lshift and r> rshift ;
