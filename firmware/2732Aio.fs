\ **********************************************************************
\
\ 2732Aio.fs is part of the 2732A EPROM Programmer project
\
\ Released as free software by permission of Tatonduk Outfitters Limited
\
\ SPDX-FileCopyrightText: 2021 Christopher Howard <christopher@librehacker.com>
\ SPDX-FileCopyrightText: 2021 Tatonduk Outfitters Limited
\ SPDX-License-Identifier: GPL-3.0-or-later
\
\ This program is free software: you can redistribute it and/or modify
\ it under the terms of the GNU General Public License as published by
\ the Free Software Foundation, either version 3 of the License, or
\ (at your option) any later version.
\
\ This program is distributed in the hope that it will be useful,
\ but WITHOUT ANY WARRANTY; without even the implied warranty of
\ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
\ GNU General Public License for more details.
\
\ You should have received a copy of the GNU General Public License
\ along with this program.  If not, see <https://www.gnu.org/licenses/>.
\
\ The purpose of this module is to handle basic setting and reading
\ operations for the EPROM pins
\
\ public words:
\   q-modes2input q-modes2output init-pin-modes write-addr-pins
\   gvpp-high gvpp-low ce-high ce-low read-q-pins write-q-pins
\ **********************************************************************

2732Aio
marker 2732Aio

\ **********************************************************************
\ init-pin-modes sets address pins, GVPP pin, and chip Enable pin to
\ output mode. Provides words for setting Q pins (eprom input/output) to
\ input or output mode. In input mode, pullup-resistor is enabled.
\ **********************************************************************

: q-modes2input
    [ %1 Q0_BT lshift ] literal Q0_DDR mclr
    [ %1 Q0_BT lshift ] literal Q0_MODE_PT mclr
    [ %1 Q1_BT lshift ] literal Q1_DDR mclr
    [ %1 Q1_BT lshift ] literal Q1_MODE_PT mclr
    [ %1 Q2_BT lshift ] literal Q2_DDR mclr
    [ %1 Q2_BT lshift ] literal Q2_MODE_PT mclr
    [ %1 Q3_BT lshift ] literal Q3_DDR mclr
    [ %1 Q3_BT lshift ] literal Q3_MODE_PT mclr
    [ %1 Q4_BT lshift ] literal Q4_DDR mclr
    [ %1 Q4_BT lshift ] literal Q4_MODE_PT mclr
    [ %1 Q5_BT lshift ] literal Q5_DDR mclr
    [ %1 Q5_BT lshift ] literal Q5_MODE_PT mclr
    [ %1 Q6_BT lshift ] literal Q6_DDR mclr
    [ %1 Q6_BT lshift ] literal Q6_MODE_PT mclr
    [ %1 Q7_BT lshift ] literal Q7_DDR mclr
    [ %1 Q7_BT lshift ] literal Q7_MODE_PT mclr
;

: q-modes2output
    [ DD_OUT Q0_BT lshift ] literal Q0_DDR mset
    [ DD_OUT Q1_BT lshift ] literal Q1_DDR mset
    [ DD_OUT Q2_BT lshift ] literal Q2_DDR mset
    [ DD_OUT Q3_BT lshift ] literal Q3_DDR mset
    [ DD_OUT Q4_BT lshift ] literal Q4_DDR mset
    [ DD_OUT Q5_BT lshift ] literal Q5_DDR mset
    [ DD_OUT Q6_BT lshift ] literal Q6_DDR mset
    [ DD_OUT Q7_BT lshift ] literal Q7_DDR mset
;

: init-pin-modes ( -- )
    [ DD_OUT A0_BT lshift ] literal A0_DDR mset
    [ DD_OUT A1_BT lshift ] literal A1_DDR mset
    [ DD_OUT A2_BT lshift ] literal A2_DDR mset
    [ DD_OUT A3_BT lshift ] literal A3_DDR mset
    [ DD_OUT A4_BT lshift ] literal A4_DDR mset
    [ DD_OUT A5_BT lshift ] literal A5_DDR mset
    [ DD_OUT A6_BT lshift ] literal A6_DDR mset
    [ DD_OUT A7_BT lshift ] literal A7_DDR mset
    [ DD_OUT A8_BT lshift ] literal A8_DDR mset
    [ DD_OUT A9_BT lshift ] literal A9_DDR mset
    [ DD_OUT A10_BT lshift ] literal A10_DDR mset
    [ DD_OUT A11_BT lshift ] literal A11_DDR mset
    [ DD_OUT GVPP_BT lshift ] literal GVPP_DDR mset
    [ DD_OUT E_BT lshift ] literal E_DDR mset
;

\ **********************************************************************
\ Sets HIGH/LOW state of output pins
\ **********************************************************************

: set-pin-output ( flag bit-num port -- )
    rot >r 1 rot rot >r lshift r> r> if mset else mclr then ;

: write-addr-pins ( u -- )
    dup #0  get-bit A0_BT  A0_PT  set-pin-output
    dup #1  get-bit A1_BT  A1_PT  set-pin-output
    dup #2  get-bit A2_BT  A2_PT  set-pin-output
    dup #3  get-bit A3_BT  A3_PT  set-pin-output
    dup #4  get-bit A4_BT  A4_PT  set-pin-output
    dup #5  get-bit A5_BT  A5_PT  set-pin-output
    dup #6  get-bit A6_BT  A6_PT  set-pin-output
    dup #7  get-bit A7_BT  A7_PT  set-pin-output
    dup #8  get-bit A8_BT  A8_PT  set-pin-output
    dup #9  get-bit A9_BT  A9_PT  set-pin-output
    dup #10 get-bit A10_BT A10_PT set-pin-output
    #11     get-bit A11_BT A11_PT set-pin-output
;

: gvpp-high ( -- ) [ #1 GVPP_BT lshift ] literal GVPP_PT mset ;

: gvpp-low ( -- ) [ #1 GVPP_BT lshift ] literal GVPP_PT mclr ;

: ce-high ( -- ) [ #1 E_BT lshift ] literal E_PT mset ;

: ce-low ( -- ) [ #1 E_BT lshift ] literal E_PT mclr ;

\ **********************************************************************
\ Reads Q (EPROM output) pins
\ **********************************************************************

: pos-q-bit ( flag bitpos -- b ) swap if #1 swap lshift else drop #0 then ;

: read-q-pins ( -- b )
    [ #1 Q0_BT lshift ] literal Q0_PINR mtst #0 pos-q-bit
    [ #1 Q1_BT lshift ] literal Q1_PINR mtst #1 pos-q-bit or
    [ #1 Q2_BT lshift ] literal Q2_PINR mtst #2 pos-q-bit or
    [ #1 Q3_BT lshift ] literal Q3_PINR mtst #3 pos-q-bit or
    [ #1 Q4_BT lshift ] literal Q4_PINR mtst #4 pos-q-bit or
    [ #1 Q5_BT lshift ] literal Q5_PINR mtst #5 pos-q-bit or
    [ #1 Q6_BT lshift ] literal Q6_PINR mtst #6 pos-q-bit or
    [ #1 Q7_BT lshift ] literal Q7_PINR mtst #7 pos-q-bit or
;

\ **********************************************************************
\ Sets Q (EPROM input) pins
\ **********************************************************************

: write-q-pins ( b -- )
    dup #0 get-bit Q0_BT Q0_PORTR set-pin-output
    dup #1 get-bit Q1_BT Q1_PORTR set-pin-output
    dup #2 get-bit Q2_BT Q2_PORTR set-pin-output
    dup #3 get-bit Q3_BT Q3_PORTR set-pin-output
    dup #4 get-bit Q4_BT Q4_PORTR set-pin-output
    dup #5 get-bit Q5_BT Q5_PORTR set-pin-output
    dup #6 get-bit Q6_BT Q6_PORTR set-pin-output
    #7     get-bit Q7_BT Q7_PORTR set-pin-output
;
