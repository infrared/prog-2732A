\ **********************************************************************
\
\ 2560defs.fs is part of the 2732A EPROM Programmer project
\
\ Released as free software by permission of Tatonduk Outfitters Limited
\
\ SPDX-FileCopyrightText: 2021 Christopher Howard <christopher@librehacker.com>
\ SPDX-FileCopyrightText: 2021 Tatonduk Outfitters Limited
\ SPDX-License-Identifier: GPL-3.0-or-later
\
\ This program is free software: you can redistribute it and/or modify
\ it under the terms of the GNU General Public License as published by
\ the Free Software Foundation, either version 3 of the License, or
\ (at your option) any later version.
\
\ This program is distributed in the hope that it will be useful,
\ but WITHOUT ANY WARRANTY; without even the implied warranty of
\ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
\ GNU General Public License for more details.
\
\ You should have received a copy of the GNU General Public License
\ along with this program.  If not, see <https://www.gnu.org/licenses/>.
\
\ The purpose of this module is to provide definitions mapping the AVR
\ pins to the EPROM pins
\
\ public words:
\   PINB PORTB DDRB PORTC DDRC PORTD DDRD PORTG DDRG PINL PORTL DDRL
\   A0_PT A0_BT A1_PT A1_BT A2_PT A2_BT A3_PT A3_BT A4_PT A4_BT A5_PT
\   A5_BT A6_PT A6_BT A7_PT A7_BT A8_PT A8_BT A9_PT A9_BT A10_PT A10_BT
\   A11_PT A11_BT Q0_PT Q0_BT Q1_PT Q1_BT Q2_PT Q2_BT Q3_PT Q3_BT Q4_PT
\   Q4_BT Q5_PT Q5_BT Q6_PT Q6_BT Q7_PT Q7_BT GVPP_PT GVPP_BT E_PT E_BT
\
\ **********************************************************************

2560defs
marker 2560defs

\ **********************************************************************
\ Port Addresses
\ **********************************************************************

$23 constant PINB
$25 constant PORTB
$24 constant DDRB
$28 constant PORTC
$27 constant DDRC
$2b constant PORTD
$2a constant DDRD
$34 constant PORTG
$33 constant DDRG
$109 constant PINL
$10b constant PORTL
$10a constant DDRL

#1 constant DD_OUT
#0 constant DD_IN

#0 constant HI-Z
#1 constant EN_PULLUP

\ **********************************************************************
\ Address pins
\ **********************************************************************

PORTL constant A0_PT
DDRL constant A0_DDR
#2 constant A0_BT
PORTL constant A1_PT
DDRL constant A1_DDR
#4 constant A1_BT
PORTL constant A2_PT
DDRL constant A2_DDR
#6 constant A2_BT
PORTG constant A3_PT
DDRG constant A3_DDR
#0 constant A3_BT
PORTG constant A4_PT
DDRG constant A4_DDR
#2 constant A4_BT
PORTC constant A5_PT
DDRC constant A5_DDR
#0 constant A5_BT
PORTC constant A6_PT
DDRC constant A6_DDR
#2 constant A6_BT
PORTC constant A7_PT
DDRC constant A7_DDR
#4 constant A7_BT
PORTC constant A8_PT
DDRC constant A8_DDR
#3 constant A8_BT
PORTC constant A9_PT
DDRC constant A9_DDR
#1 constant A9_BT
PORTG constant A10_PT
DDRG constant A10_DDR
#1 constant A10_BT
PORTD constant A11_PT
DDRD constant A11_DDR
#7 constant A11_BT

\ **********************************************************************
\ "Q" pins - memory data in and out
\ **********************************************************************

PINL constant Q0_PINR
PORTL constant Q0_MODE_PT
PORTL constant Q0_PORTR
DDRL constant Q0_DDR
#0 constant Q0_BT
PINB constant Q1_PINR
PORTB constant Q1_MODE_PT
PORTB constant Q1_PORTR
DDRB constant Q1_DDR
#2 constant Q1_BT
PINB constant Q2_PINR
PORTB constant Q2_MODE_PT
PORTB constant Q2_PORTR
DDRB constant Q2_DDR
#0 constant Q2_BT
PINB constant Q3_PINR
PORTB constant Q3_MODE_PT
PORTB constant Q3_PORTR
DDRB constant Q3_DDR
#1 constant Q3_BT
PINB constant Q4_PINR
PORTB constant Q4_MODE_PT
PORTB constant Q4_PORTR
DDRB constant Q4_DDR
#3 constant Q4_BT
PINL constant Q5_PINR
PORTL constant Q5_MODE_PT
PORTL constant Q5_PORTR
DDRL constant Q5_DDR
#1 constant Q5_BT
PINL constant Q6_PINR
PORTL constant Q6_MODE_PT
PORTL constant Q6_PORTR
DDRL constant Q6_DDR
#3 constant Q6_BT
PINL constant Q7_PINR
PORTL constant Q7_MODE_PT
PORTL constant Q7_PORTR
DDRL constant Q7_DDR
#5 constant Q7_BT

\ **********************************************************************
\ Chip enable and programming pins
\ **********************************************************************

PORTC constant GVPP_PT
DDRC constant GVPP_DDR
#5 constant GVPP_BT
PORTL constant E_PT
DDRL constant E_DDR
#7 constant E_BT
