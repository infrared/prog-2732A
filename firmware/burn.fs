\ **********************************************************************
\
\ burn.fs is part of the 2732A EPROM Programmer project
\
\ Released as free software by permission of Tatonduk Outfitters Limited
\
\ SPDX-FileCopyrightText: 2021 Christopher Howard <christopher@librehacker.com>
\ SPDX-FileCopyrightText: 2021 Tatonduk Outfitters Limited
\ SPDX-License-Identifier: GPL-3.0-or-later
\
\ This program is free software: you can redistribute it and/or modify
\ it under the terms of the GNU General Public License as published by
\ the Free Software Foundation, either version 3 of the License, or
\ (at your option) any later version.
\
\ This program is distributed in the hope that it will be useful,
\ but WITHOUT ANY WARRANTY; without even the implied warranty of
\ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
\ GNU General Public License for more details.
\
\ You should have received a copy of the GNU General Public License
\ along with this program.  If not, see <https://www.gnu.org/licenses/>.
\
\ The purpose of this module is to handle executing a single
\ burn/verify cycle, and setup for first burn. Also provides words just
\ for reading memory.
\
\ public words:
\   first-burn-init burn/verify first-read-init read
\
\ **********************************************************************

burn
marker burn

\ **********************************************************************
\ Tweakable parameters:
\
\   PULSE_WIDTH: burn pulse width in milliseconds. This is not the
\   same across different models of 2732A. 2732A-45UL is 10 ms
\
\   GVPP_SETTLE: In testing on my REV C design, it takes some time for
\   gvpp to settle back down to GND. With 47k resistor between GVPP to
\   GND, 50 milliseconds seems to have worked consistently. 10 ms had
\   also worked in some tests.
\ **********************************************************************

#10 constant PULSE_WIDTH

#50 constant GVPP_SETTLE

\ **********************************************************************
\ `first-burn-init' sets up pin direction for the pins that don't
\ change, and ensure CE pin is high. It also sets GVPP low, though if
\ GVPP wasn't already low, probably you've damaged something.
\ **********************************************************************

: first-burn-init ( -- ) init-pin-modes gvpp-low ce-high ;

\ **********************************************************************
\ Helper words for a single burn/verify cycle
\ **********************************************************************

: setup-burn ( b u -- )
    write-addr-pins q-modes2output write-q-pins gvpp-high #1 ms ;

: pulse-ce ce-low PULSE_WIDTH ms ce-high ;

: prep-verify #1 ms q-modes2input gvpp-low GVPP_SETTLE ms ;

: verify ce-low #1 ms read-q-pins ;

: verify-cleanup ce-high #1 ms ;

\ **********************************************************************
\ Single-byte burn/verify cycle. Assumes you have already
\ ran `first-burn-init' at least once.
\ **********************************************************************

: burn/verify ( b u -- b )
    setup-burn pulse-ce prep-verify verify verify-cleanup ;

\ **********************************************************************
\ Words for reading only.
\ **********************************************************************

: first-read-init ( -- ) first-burn-init ;

: read ( u -- b ) write-addr-pins prep-verify verify verify-cleanup ;
